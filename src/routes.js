import { Router } from 'express';
import multer from 'multer';
import multerConfig from './config/multer';

import UserController from './app/controllers/User';
import SessionController from './app/controllers/Session';
import FileController from './app/controllers/File';
import auth from './app/middleware/auth';

const routes = new Router();
const upload = multer(multerConfig);

// session
routes.post('/session', SessionController.store);
routes.post('/user', UserController.store);
routes.use(auth);

routes.put('/user', UserController.update);

// files
routes.post('/files', upload.single('file'), FileController.store);

export default routes;
